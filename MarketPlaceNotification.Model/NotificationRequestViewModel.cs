﻿using System;

namespace MarketPlaceNotification.Model
{
    public class NotificationRequestViewModel
    {
        public long NotificationID { get; set; }
        public bool IsSent { get; set; }
    }
}
