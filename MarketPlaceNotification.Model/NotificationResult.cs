﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MarketPlaceNotification.Model
{
    public class NotificationResult
    {
        public string success { get; set; }
        public string failure { get; set; }
        public List<ErrorResult> results { get; set; }
    }

    public class ErrorResult
    {
        public string error { get; set; }
    }
}
