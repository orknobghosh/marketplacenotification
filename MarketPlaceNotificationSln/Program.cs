﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MarketPlaceNotification.Domain;

namespace MarketPlaceNotificationSln
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("-----------------Notification Process Starts-----------------\n");

            bool sentFlag = new NotificationManager().GetSendUsentNotification();
            string status = sentFlag ? "SUCCESS" : "FAILED";
            Console.WriteLine("Email Sent Status {0} \n", status);
            Console.WriteLine("-----------------Notification Process Ends-----------------\n");
            Console.ReadKey();
        }
    }
}
