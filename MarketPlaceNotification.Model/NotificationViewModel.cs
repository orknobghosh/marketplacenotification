﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MarketPlaceNotification.Model
{
    public class NotificationViewModel
    {
        public long NotificationID { get; set; }
        public string Description { get; set; }
        public string FileName { get; set; }
        public string ImageUrl { get; set; }
        public bool IsSent { get; set; }
        public string NotificationType { get; set; }
        public string Title { get; set; }

        public string ServerKey { get; set; }

        public string SenderID { get; set; }
        public string IsSound { get; set; }

        public string Url { get; set; }

        public List<UserFirebaseList> UserFirebaseList { get; set; }
    }


    public class Result
    {
        public NotificationData data;
    }

    public class NotificationData
    {
        public NotificationUserFirebaseJson notificationUserFirebaseJson { get; set; }
    }

    public class NotificationUserFirebaseJson
    {
        public List<NotificationViewModel> NotificationUserList { get; set; }

    }
    public class UserFirebaseList
    {
        public long UserID { get; set; }
        public string FirebaseToken { get; set; }
    }

    public class UpdateResult
    {
        public long data { get; set; }
    }
}
