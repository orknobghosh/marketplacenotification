﻿using System;
using System.Collections.Generic;
using System.Linq;
using MarketPlaceNotification.Model;
using MarketPlaceNotification.Repository;
using MarketPlaceNotification.Service;

namespace MarketPlaceNotification.Domain
{
    public class NotificationManager
    {
        public bool GetSendUsentNotification()
        {
            Console.WriteLine("Get Unsent users notification list - ORDER \n");
            bool sentFlag = false;
            long flagCount = 0;


            NotificationRepository notificationRepository = new NotificationRepository();
            NotificationData data = notificationRepository.GetUnsentNotifications();


            if (data==null)
            {
                
                Console.WriteLine("NO PENDING NOTIFICATIONS AS THE COUNT IS ZERO \n");
                return false;

            }

            var notificationList = data.notificationUserFirebaseJson.NotificationUserList;
            Console.WriteLine("Unsent users  notification Count {0}", notificationList.Count.ToString());
            if (notificationList.Count > 0)
            {


                foreach (var notification in notificationList)
                {

                    foreach(var userfirebase in notification.UserFirebaseList)
                    {
                        sentFlag = new NotificationService().SendMessage(message: notification.Description, title: notification.Title, isSound: notification.IsSound
                            , to: userfirebase.FirebaseToken,url:notification.Url,
                            serverKey:notification.ServerKey,senderId:notification.SenderID);

                        Console.WriteLine("User ID {0} - Sent Status {1} -",userfirebase.UserID.ToString(),sentFlag.ToString());
                    }
                                                            
                    NotificationRequestViewModel model = new NotificationRequestViewModel
                    {
                        NotificationID  = notification.NotificationID,
                        IsSent = sentFlag
                    };
                    flagCount = new NotificationRepository().UpdateSentFlag(model);

                    flagCount++;
                }

            }


            return flagCount > 0 ? true : false;
        }
    }
}
