﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using MarketPlaceNotification.Model;
using Newtonsoft.Json;

namespace MarketPlaceNotification.Repository
{
    public class NotificationRepository
    {



        NotificationData data = new NotificationData();

        public NotificationData GetUnsentNotifications()
        {
            return GetUnsentNotificationsAPI();

        }

        private NotificationData GetUnsentNotificationsAPI()
        {

            try
            {
                HttpClient cons = new HttpClient();
                cons.BaseAddress = new Uri("http://lesela-my-market-place-api-dev.eu-central-1.elasticbeanstalk.com/");
                cons.DefaultRequestHeaders.Accept.Clear();
                cons.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                using (cons)
                {

                    HttpResponseMessage res = cons.PostAsync("api/notification/unsentnotification", null).Result;
                    res.EnsureSuccessStatusCode();
                    if (res.IsSuccessStatusCode)
                    {
                        var messageJsonString = res.Content.ReadAsStringAsync();
                        var messageLists = JsonConvert.DeserializeObject<Result>(messageJsonString.Result);
                        data = messageLists.data;
                        Console.WriteLine("\n");
                        Console.WriteLine("\n");
                        Console.WriteLine("-----------------------------------------------------------");
                        Console.WriteLine("------------------Calling Post Operation--------------------");
                        Console.WriteLine("------------------Created Successfully--------------------");
                    }

                    return data;
                }

            }


            catch (Exception ex)
            {
                throw ex;
            }




        }


        public long UpdateSentFlag(NotificationRequestViewModel model)
        {
            return UpdateSentFlagAPI(model);
        }

        private long UpdateSentFlagAPI(NotificationRequestViewModel model)
        {
            try
            {

                long flagCount = 0;
                HttpClient cons = new HttpClient();
                cons.BaseAddress = new Uri("http://lesela-my-market-place-api-dev.eu-central-1.elasticbeanstalk.com/");
                cons.DefaultRequestHeaders.Accept.Clear();
                cons.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                using (cons)
                {
                    var content = JsonConvert.SerializeObject(model);
                    var buffer = System.Text.Encoding.UTF8.GetBytes(content);
                    var byteContent = new ByteArrayContent(buffer);
                    byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                    HttpResponseMessage res = cons.PostAsync("api/notification/updatenoticationsentflag", byteContent).Result;
                    res.EnsureSuccessStatusCode();
                    if (res.IsSuccessStatusCode)
                    {
                        var messageJsonString = res.Content.ReadAsStringAsync();
                        var messageLists = JsonConvert.DeserializeObject<UpdateResult>(messageJsonString.Result);
                        flagCount = messageLists.data;
                        Console.WriteLine("\n");
                        Console.WriteLine("\n");
                        Console.WriteLine("-----------------------------------------------------------");
                        Console.WriteLine("------------------Calling Post Operation--------------------");
                        Console.WriteLine("------------------Created Successfully--------------------");
                    }

                    return flagCount;
                }

            }


            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
