﻿using System;
using System.IO;
using System.Net;
using MarketPlaceNotification.Model;
using Newtonsoft.Json;

namespace MarketPlaceNotification.Service
{
    public class NotificationService
    {
        public bool SendMessage(string message, string title, string isSound, string to,string url,string serverKey, string senderId)
        {
            
            var result = "-1";
            bool flag = false;
            var notificationResult = new NotificationResult();
          

            try
            {

               
                var webAddr = "https://fcm.googleapis.com/fcm/send";

                var httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddr);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Headers.Add("Authorization:key=" + serverKey);
                httpWebRequest.Headers.Add("Sender:id=" + senderId);
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = "{\"to\": \"" + to + "\",\"data\": {\"message\": \"" + message + "\",\"title\": \"" + title + "\",\"sound\": \"" + isSound + "\",\"url\": \"" + url + "\",}}";

                    streamWriter.Write(json);
                    streamWriter.Flush();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                    notificationResult = JsonConvert.DeserializeObject<NotificationResult>(result);
                }

                // return result;
            }
            catch
            {
                throw;
            }

            if(notificationResult.success=="1")
            {
                flag= true;
            }
            else if(notificationResult.failure=="1")
            {
                flag= false;
            }
            return flag;
        }
    }
}
